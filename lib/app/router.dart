import 'package:auto_route/auto_route_annotations.dart';
import 'package:movi_utp/src/ui/views/views.dart';

import '../main.dart';

@MaterialAutoRouter(routes: [
  MaterialRoute(page: HomeCardRoute),
  MaterialRoute(page: AboutAppSimpleRoute),
  MaterialRoute(page: StepperRoute),
  MaterialRoute(page: MonitoriaRoute),
  MaterialRoute(page: AsincronasRoute),
  MaterialRoute(page: GasometricaRoute),
  MaterialRoute(page: CalculoGasometricaRoute),
  MaterialRoute(page: ParametrosPresionRoute),
  MaterialRoute(page: CalculoPresionRoute),
  MaterialRoute(page: CambiodePaCO2Route),
  MaterialRoute(page: CalculoCambiodePaCO2Route),
  MaterialRoute(page: SplashScreen),
])
class $Router {}
