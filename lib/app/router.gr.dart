// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import '../main.dart';
import '../src/ui/views/views.dart';

class Routes {
  static const String homeCardRoute = '/home-card-route';
  static const String aboutAppSimpleRoute = '/about-app-simple-route';
  static const String stepperRoute = '/stepper-route';
  static const String monitoriaRoute = '/monitoria-route';
  static const String asincronasRoute = '/asincronas-route';
  static const String gasometricaRoute = '/gasometrica-route';
  static const String calculoGasometricaRoute = '/calculo-gasometrica-route';
  static const String parametrosPresionRoute = '/parametros-presion-route';
  static const String calculoPresionRoute = '/calculo-presion-route';
  static const String cambiodePaCO2Route = '/cambiode-pa-co2-route';
  static const String calculoCambiodePaCO2Route =
      '/calculo-cambiode-pa-co2-route';
  static const String splashScreen = '/splash-screen';
  static const all = <String>{
    homeCardRoute,
    aboutAppSimpleRoute,
    stepperRoute,
    monitoriaRoute,
    asincronasRoute,
    gasometricaRoute,
    calculoGasometricaRoute,
    parametrosPresionRoute,
    calculoPresionRoute,
    cambiodePaCO2Route,
    calculoCambiodePaCO2Route,
    splashScreen,
  };
}

class Router extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.homeCardRoute, page: HomeCardRoute),
    RouteDef(Routes.aboutAppSimpleRoute, page: AboutAppSimpleRoute),
    RouteDef(Routes.stepperRoute, page: StepperRoute),
    RouteDef(Routes.monitoriaRoute, page: MonitoriaRoute),
    RouteDef(Routes.asincronasRoute, page: AsincronasRoute),
    RouteDef(Routes.gasometricaRoute, page: GasometricaRoute),
    RouteDef(Routes.calculoGasometricaRoute, page: CalculoGasometricaRoute),
    RouteDef(Routes.parametrosPresionRoute, page: ParametrosPresionRoute),
    RouteDef(Routes.calculoPresionRoute, page: CalculoPresionRoute),
    RouteDef(Routes.cambiodePaCO2Route, page: CambiodePaCO2Route),
    RouteDef(Routes.calculoCambiodePaCO2Route, page: CalculoCambiodePaCO2Route),
    RouteDef(Routes.splashScreen, page: SplashScreen),
  ];
  @override
  Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, AutoRouteFactory>{
    HomeCardRoute: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => HomeCardRoute(),
        settings: data,
      );
    },
    AboutAppSimpleRoute: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => AboutAppSimpleRoute(),
        settings: data,
      );
    },
    StepperRoute: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => StepperRoute(),
        settings: data,
      );
    },
    MonitoriaRoute: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => MonitoriaRoute(),
        settings: data,
      );
    },
    AsincronasRoute: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const AsincronasRoute(),
        settings: data,
      );
    },
    GasometricaRoute: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const GasometricaRoute(),
        settings: data,
      );
    },
    CalculoGasometricaRoute: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => CalculoGasometricaRoute(),
        settings: data,
      );
    },
    ParametrosPresionRoute: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const ParametrosPresionRoute(),
        settings: data,
      );
    },
    CalculoPresionRoute: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => CalculoPresionRoute(),
        settings: data,
      );
    },
    CambiodePaCO2Route: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const CambiodePaCO2Route(),
        settings: data,
      );
    },
    CalculoCambiodePaCO2Route: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => CalculoCambiodePaCO2Route(),
        settings: data,
      );
    },
    SplashScreen: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => SplashScreen(),
        settings: data,
      );
    },
  };
}
