part of ui.shared;

class UIColors {
  /// [Primary-Group]
  ///

  static const Color cyanMedium = Color(0xFFB2EBF2);
  static const Color redMedium = Color(0xFFffcdd2);
}
