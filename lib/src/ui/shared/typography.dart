part of ui.shared;

/// [Headers-group]
TextStyle headers = TextStyle(
  letterSpacing: 0,
);
TextStyle h0 = headers.copyWith(fontSize: 40, fontWeight: FontWeight.w500);
TextStyle h1 = headers.copyWith(fontSize: 32, fontWeight: FontWeight.w500);
TextStyle h2 = headers.copyWith(fontSize: 28, fontWeight: FontWeight.w500);
TextStyle h3 = headers.copyWith(fontSize: 24, fontWeight: FontWeight.w500);
TextStyle h4 = headers.copyWith(fontSize: 20, fontWeight: FontWeight.w600);
TextStyle h6 = headers.copyWith(fontSize: 16, fontWeight: FontWeight.w500);
TextStyle h7 = headers.copyWith(fontSize: 12, fontWeight: FontWeight.w500);
TextStyle h8 = headers.copyWith(fontSize: 10, fontWeight: FontWeight.w500);

/// [Short-Statements-Group]
TextStyle shortStatements = TextStyle(
  letterSpacing: 0,
  fontWeight: FontWeight.bold,
);

TextStyle bodyLargeBold = shortStatements.copyWith(fontSize: 32);
TextStyle bodyMediumBold = shortStatements.copyWith(fontSize: 20);
TextStyle bodySmallBold = shortStatements.copyWith(fontSize: 16);
TextStyle bodyXSmallBold = shortStatements.copyWith(fontSize: 12);

TextStyle error = TextStyle(
  color: Colors.red,
  letterSpacing: 0,
);
TextStyle errorSmall = error.copyWith(fontSize: 12);
TextStyle errorMedium = error.copyWith(fontSize: 14);
TextStyle errorLarge = error.copyWith(fontSize: 16);

/// [Paragraph-Group]
TextStyle paragraph = TextStyle(
  letterSpacing: 0,
  // height: 1.8,
);

TextStyle bodyLargeRegular = paragraph.copyWith(fontSize: 32);
TextStyle bodyMediumRegular = paragraph.copyWith(fontSize: 20);
TextStyle bodySmallRegular = paragraph.copyWith(fontSize: 16);
TextStyle bodyXSmallRegular = paragraph.copyWith(fontSize: 12);

/// [Paragraph-Group]
TextStyle paragraphLight = TextStyle(
    fontFamily: 'Ubuntu-Light', letterSpacing: 0, fontWeight: FontWeight.w400);

TextStyle bodyMediumLight = paragraphLight.copyWith(fontSize: 20);
TextStyle bodySmallLight = paragraphLight.copyWith(fontSize: 16);
TextStyle bodyXSmallLight = paragraphLight.copyWith(fontSize: 12);

/// [Links-Group]
TextStyle linkBase = TextStyle(
  fontWeight: FontWeight.w700,
  letterSpacing: 0,
  decoration: TextDecoration.underline,
);
TextStyle linkLarge = linkBase.copyWith(fontSize: 18);
TextStyle linkMedium = linkBase.copyWith(fontSize: 16);
TextStyle linkSmall = linkBase.copyWith(fontSize: 14);
TextStyle linkDisclaimer = linkBase.copyWith(fontSize: 12);

/// [Button-Group]
TextStyle buttonBase =
    TextStyle(fontWeight: FontWeight.w500, letterSpacing: 0, fontSize: 16);
TextStyle buttonSmall = buttonBase.copyWith(fontSize: 12);

/// [Money-Grop]
TextStyle moneyBase = TextStyle(
  fontWeight: FontWeight.w800,
  fontSize: 16,
  letterSpacing: 0,
);
