part of ui.shared;

class UILayout {
  static const double xsmall = 4;
  static const double small = 8;
  static const double medium = 16;
  static const double large = 24;
  static const double xlarge = 48;
  static const double xxlarge = 96;

  static const double bottomMargin = 100;

  static const double svgImages = 182;

  static const double aspectRatio_16_9 = 16 / 9;
  static const double aspectRatio_3_2 = 3 / 2;
  static const double aspectRatio_4_3 = 4 / 3;
  static const double aspectRatio_1_1 = 1 / 1;
  static const double aspectRatio_3_3 = 3 / 4;
  static const double aspectRatio_2_3 = 2 / 3;
}
