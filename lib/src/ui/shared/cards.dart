part of ui.shared;

class UiCards {
  static Widget displayCardTwo(String imageSource,
          {Function function, String titleText, String descriptionText}) =>
      Padding(
        padding: UIPadding.paddingH4,
        child: Card(
          elevation: 3,
          margin: EdgeInsets.symmetric(vertical: 16),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 16, horizontal: 8),
            child: Column(
              children: [
                Text(
                  titleText ?? '',
                  style: bodySmallBold,
                ),
                UISpacing.spacingV8,
                Image.asset(imageSource),
                UISpacing.spacingV8,
                Text(
                  descriptionText ?? '',
                  style: bodySmallRegular,
                ),
                RaisedButton(
                  color: Colors.blue,
                  elevation: 5,
                  onPressed: function,
                  child: Text('Acciones',
                      style: bodySmallBold.copyWith(color: Colors.white)),
                )
              ],
            ),
          ),
        ),
      );

  static Widget displayCard(bool perfusion,
          {Color backgroundColor,
          String leadingText,
          String subLeadingText,
          String trailingText,
          String measurement,
          String errorMsg,
          String vrText}) =>
      Container(
        color: backgroundColor,
        child: Padding(
          padding: EdgeInsets.only(left: 8, right: 8, bottom: 12),
          child: Card(
            elevation: 5,
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            leadingText,
                            style: bodySmallBold,
                            textAlign: TextAlign.left,
                          ),
                          UISpacing.spacingV8,
                          Text(
                            subLeadingText,
                            style: bodyXSmallRegular,
                            textAlign: TextAlign.left,
                          ),
                        ],
                      ),
                      Spacer(),
                      Column(
                        children: [
                          Text(
                            trailingText,
                            style: h2,
                          ),
                          if (measurement != null)
                            Text(measurement,
                                style: h7.copyWith(color: Colors.grey)),
                          if (vrText != null)
                            Text(
                              vrText,
                              style: h4.copyWith(color: Colors.green),
                            ),
                        ],
                      )
                    ],
                  ),
                  if (errorMsg != null) ...[
                    UISpacing.spacingV8,
                    Row(
                      children: [
                        Icon(
                          Icons.info_outline,
                          color: Colors.red,
                        ),
                        UISpacing.spacingH8,
                        Text(
                          errorMsg,
                          style: bodySmallRegular.copyWith(color: Colors.red),
                        )
                      ],
                    ),
                  ]
                ],
              ),
            ),
          ),
        ),
      );
}
