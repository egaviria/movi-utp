part of ui.shared;

class Constantes {
  static const List<List> parametros = [
    ['D(A-a)O2', 'gradiente alveolo \n arterial de oxígeno'],
    ['IaA', 'índice arterio alveolar'],
    [
      'PAFI',
      'presión arterial de oxígeno \n y la fracción inspirada de oxígeno'
    ],
    ['Qs/Qt', 'porcentaje de Shunt'],
    ['D(a-v)O2', 'gradiente arterio-venoso \n de oxígeno'],
    ['ExtO2', 'Tasa de extracción'],
    ['SvO2', 'saturación venosa de oxígeno'],
    ['DeltaCO2', 'diferencia arterio-venosa de CO2'],
  ];
  static const List<String> iaA = ['IaA', 'índice arterio alveolar'];
}
