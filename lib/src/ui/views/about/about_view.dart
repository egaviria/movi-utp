part of app.ui.views;

class AboutAppSimpleRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<AboutAppSimpleRouteViewModel>.reactive(
      builder: (context, AboutAppSimpleRouteViewModel model, child) => Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 0,
            title: Text('About', style: TextStyle(color: MyColors.grey_80)),
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: MyColors.grey_80),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            actions: <Widget>[
              IconButton(
                icon: const Icon(Icons.search, color: MyColors.grey_80),
                onPressed: () {},
              )
            ]),
        body: Container(
          width: double.infinity,
          height: double.infinity,
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(horizontal: 35),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text('MOVI App',
                  style: MyText.display1(context).copyWith(
                      color: MyColors.grey_60, fontWeight: FontWeight.w300)),
              Container(height: 5),
              Container(width: 120, height: 3, color: Colors.teal[300]),
              Container(height: 15),
              Text('Version',
                  style:
                      MyText.body1(context).copyWith(color: MyColors.grey_40)),
              Text('1.0',
                  style:
                      MyText.medium(context).copyWith(color: MyColors.grey_90)),
              Container(height: 15),
              Text('Last Update',
                  style:
                      MyText.body1(context).copyWith(color: MyColors.grey_40)),
              Text('Agosto 2020',
                  style:
                      MyText.medium(context).copyWith(color: MyColors.grey_90)),
              Container(height: 25),
              Text(
                  'La monitorización de las propiedades mecánicas (tanto estáticas como dinámicas) del aparato respiratorio es imprescindible para el diagnóstico y pronóstico de la enfermedad causante de cualquier proceso, así como para tomar las decisiones terapéuticas pertinentes',
                  style:
                      MyText.medium(context).copyWith(color: MyColors.grey_90)),
              Container(height: 25),
              Text('UTP',
                  style:
                      MyText.medium(context).copyWith(color: MyColors.grey_90)),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.teal[300],
          onPressed: () {
            Navigator.pop(context);
          },
          child: Icon(Icons.keyboard_return),
        ),
      ),
      viewModelBuilder: () => AboutAppSimpleRouteViewModel(),
    );
  }
}
