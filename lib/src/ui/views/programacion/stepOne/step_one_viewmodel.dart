part of app.ui.views;

class StepOneViewModel extends BaseViewModel {
  final TextEditingController _inputController = TextEditingController();
  bool _isMan = true;
  FocusNode _focusNode;
  bool get isMan => _isMan;

  double _pesoIdeal = 0;

  double get pesoIdeal => _pesoIdeal;

  void _getImc(String data) {
    if (data.length <= 1) {
      _resetValues();
      return;
    }

    if (_isMan == true) {
      _getManImc(data);
    } else {
      _getWomanImc(data);
    }
  }

  void _resetValues() {
    _pesoIdeal = 0;
    notifyListeners();
  }

  Text _getTitleText(String text) => Text(
        text,
        style: TextStyle(color: MyColors.accent, fontSize: 18.0),
      );

  TextField _getInput() {
    return TextField(
      autofocus: false,
      focusNode: _focusNode,
      controller: _inputController,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
          prefixIcon: Icon(Icons.unfold_more),
          labelText: 'Talla (cm)'),
      onChanged: (data) {
        _getImc(data);
      },
    );
  }

  void _getManImc(String data) {
    var long2 = double.parse('$data');

    print('_getManImc');
    print(long2);
    _pesoIdeal = ((long2 - 152.4) * 0.91) + 50;
    print(_pesoIdeal);
    notifyListeners();
  }

  void _getWomanImc(String data) {
    var long2 = double.parse('$data');

    print('_getWomanImc');
    _pesoIdeal = ((long2 - 152.4) * 0.91) + 45.5;
    notifyListeners();
  }

  Widget _getManWomanButton(BuildContext context, String text) {
    if (_isMan == true && text == 'Hombre' ||
        _isMan == false && text == 'Mujer') {
      return RaisedButton(
        onPressed: () {},
        color: MyColors.primary,
        child: Text(
          text,
          style: TextStyle(color: Colors.white),
        ),
      );
    } else {
      return OutlineButton(
        onPressed: () {
          print(_inputController.text);
          _isMan = !_isMan;
          if (_inputController.text != '') {
            FocusScope.of(context).unfocus();
            _getImc(_inputController.text);
          }
          notifyListeners();
        },
        color: MyColors.primary,
        child: Text(
          text,
        ),
      );
    }
  }
}
