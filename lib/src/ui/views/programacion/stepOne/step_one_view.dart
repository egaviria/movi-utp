part of app.ui.views;

class StepOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<StepOneViewModel>.reactive(
        builder: (context, model, child) => Padding(
              padding: EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(bottom: 16.0, right: 16),
                    child: Text(
                      'Peso Ideal y volumen tidal ',
                      style: TextStyle(fontSize: 24.0),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 16.0),
                    child: Text(
                      'Ingrese la talla del paciente  y obtenga el Volumen corriente ',
                      style: TextStyle(fontSize: 16.0),
                    ),
                  ),
                  model._getInput(),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                            child: model._getManWomanButton(context, 'Hombre')),
                        Expanded(
                            child: model._getManWomanButton(context, 'Mujer')),
                      ],
                    ),
                  ),
                  Expanded(
                    child: ListView(
                      children: [
                        Divider(),
                        ListTile(
                          title: model._getTitleText('Peso Ideal'),
                          trailing:
                              Text(model.pesoIdeal.round().toString() + ' Kg'),
                        ),
                        Divider(),
                        ListTile(
                          title: model._getTitleText('6 ml/kg'),
                          trailing:
                              Text((model.pesoIdeal * 6).round().toString()),
                        ),
                        Divider(),
                        ListTile(
                          title: model._getTitleText('7 ml/kg'),
                          trailing:
                              Text((model.pesoIdeal * 7).round().toString()),
                        ),
                        Divider(),
                        ListTile(
                          title: model._getTitleText('8 ml/kg'),
                          trailing:
                              Text((model.pesoIdeal * 8).round().toString()),
                        ),
                        Divider(),
                      ],
                    ),
                  )
                ],
              ),
            ),
        viewModelBuilder: () => StepOneViewModel());
  }
}
