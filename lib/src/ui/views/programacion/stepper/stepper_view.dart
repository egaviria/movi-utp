part of app.ui.views;

class StepperRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<StepperViewModel>.reactive(
      viewModelBuilder: () => StepperViewModel(),
      onModelReady: (StepperViewModel model) => null,
      builder: (context, model, child) => Scaffold(
        backgroundColor: Colors.grey[100],
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(0),
            child: Container(color: Colors.grey[100])),
        body: Container(
          width: double.infinity,
          height: double.infinity,
          child: Column(children: <Widget>[
            Expanded(
              child: Stack(
                children: <Widget>[
                  PageView(
                    onPageChanged: model.onPageViewChange,
                    controller: model.pageController,
                    children: _getSteps(context, model),
                  ),
                  Row(
                    children: <Widget>[
                      Spacer(),
                      IconButton(
                        icon: Icon(Icons.close, color: Colors.grey[400]),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  )
                ],
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                height: 30,
                child: Align(
                  alignment: Alignment.topCenter,
                  child: model.buildDots(context),
                ),
              ),
            ),
            // Text('sdraProvider.sdra ${model.sdraProvider.sdra}'),
            Container(
              width: double.infinity,
              height: 50,
              child: FlatButton(
                color: Colors.grey[200],
                onPressed: () {
                  if (model.isLast) {
                    Navigator.pop(context);
                    return;
                  }
                  model.pageController.nextPage(
                      duration: Duration(milliseconds: 300),
                      curve: Curves.easeOut);
                },
                child: Text(model.isLast ? 'TERMINAR' : 'SIGUIENTE'),
              ),
            )
          ]),
        ),
      ),
    );
  }

  List<Widget> _getSteps(BuildContext context, StepperViewModel model) {
    List<Widget> items = [
      StepOne(),
    ];

    for (int i = 0; i < model.wizardTitle.length; i++) {
      StepModel obj = StepModel();
      obj.image = model.wizardImage[i];
      obj.title = model.wizardTitle[i];
      obj.brief = model.sdraProvider.sdra
          ? model.wizardBriefConSDRA[i]
          : model.wizardBriefSinSDRA[i];
      items.add(buildPageViewItem(context, obj, model));
    }

    return items;
  }

  Widget buildPageViewItem(
      BuildContext context, StepModel wz, StepperViewModel model) {
    return Container(
      padding: EdgeInsets.all(35),
      alignment: Alignment.center,
      width: double.infinity,
      height: double.infinity,
      child: Wrap(
        children: <Widget>[
          Container(
              width: double.infinity,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(35),
                    child:
                        Image.asset(Img.get(wz.image), width: 150, height: 150),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(child: _getSDRAButton(context, 'Sin', model)),
                        Expanded(child: _getSDRAButton(context, 'Con', model)),
                      ],
                    ),
                  ),
                  Text(wz.title,
                      style: TextStyle(
                          fontSize: 18.0,
                          color: Colors.grey[800],
                          fontWeight: FontWeight.bold)),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 25),
                    child: Text(wz.brief,
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.grey[600])),
                  ),
                ],
              ))
        ],
      ),
    );
  }

  Widget _getSDRAButton(
      BuildContext context, String text, StepperViewModel model) {
    print('_getSDRAButton');

    if (model.sdraProvider.sdra == true && text == 'Con' ||
        model.sdraProvider.sdra == false && text == 'Sin') {
      return RaisedButton(
        onPressed: () {},
        color: MyColors.primary,
        child: Text(
          text + ' SDRA',
          style: TextStyle(color: Colors.white),
        ),
      );
    } else {
      return OutlineButton(
        onPressed: () {
          model.sdraProvider.switchSdra();
        },
        color: MyColors.primary,
        child: Text(
          text + ' SDRA',
        ),
      );
    }
  }
}
