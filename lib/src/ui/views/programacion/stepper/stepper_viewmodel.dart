part of app.ui.views;

class StepperViewModel extends ReactiveViewModel {
  final sdraProvider = GetIt.instance.get<SdraProvider>();

  final List<Widget> _items = [];
  PageController pageController = PageController(
    initialPage: 0,
  );
  int page = 0;
  bool isLast = false;

  final List<String> wizardTitle = [
    'Modo Asistocontrolado',
    'Programar Fio2 y aplicar PEEP',
    'Programar flujo',
    'Monitorizar'
  ];

  final List<String> wizardBriefSinSDRA = [
    'Programar modo Asistocontrolado por volumen aplicar 6-8 ml/kg de peso ideal',
    'Programar Fio2 ≥ 21% para SaO2 ≥  94% y aplicar PEEP de 5 -8 cmH20',
    'Programar flujo ≥ 60 L/min  y mantener relación I:E de 1:2.  con frecuencia respiratoria entre 12 y 25 respiraciones por minuto   ',
    'Monitorizar la Ventilaciòn mecánica ',
  ];

  final List<String> wizardBriefConSDRA = [
    'Programar modo Asistocontrolado por volumen aplicar 6 ml/kg de peso ideal',
    'Programar Fio2 ≥ 21% para SaO2 ≥  94% y aplicar PEEP de 5 -8 cmH20',
    'Programar flujo ≥ 60 L/min  y mantener relación I:E de 1:2.  con frecuencia respiratoria entre 12 y 25 respiraciones por minuto   ',
    'Medir Driving pressure, Presiòn meseta . Mantener Plateau <28 cmh20 y Driving pressure <15. Si Driving pressure>15 disminuir volumen corriente, ajustar PEEP según tabla PEEP FioO2 ',
  ];

  final List<String> wizardImage = [
    '013-pills.png',
    '019-respirator.png',
    '019-water glass.png',
    '030-medical app.png',
  ];

  @override
  List<ReactiveServiceMixin> get reactiveServices =>
      [sdraProvider]; // add counter service as reactive service

  void onPageViewChange(int _page) {
    page = _page;
    isLast = _page == _items.length - 1;
    notifyListeners();
  }

  Widget buildDots(BuildContext context) {
    Widget widget;

    List<Widget> dots = [];
    for (int i = 0; i < _items.length; i++) {
      Widget w = Container(
        margin: EdgeInsets.symmetric(horizontal: 5),
        height: 8,
        width: 8,
        child: CircleAvatar(
          backgroundColor: page == i ? Colors.orange[400] : Colors.grey[200],
        ),
      );
      dots.add(w);
    }
    widget = Row(
      mainAxisSize: MainAxisSize.min,
      children: dots,
    );
    return widget;
  }

  // List<Widget> _getSteps(BuildContext context) {

  //   List<Widget> items = [
  //     StepOne(),
  //   ];

  //   for (int i = 0; i < wizard_title.length; i++) {
  //     StepModel obj = new StepModel();
  //     obj.image = wizard_image[i];
  //     obj.title = wizard_title[i];
  //     obj.brief =
  //         provider.sdra ? wizard_brief_consdra[i] : wizard_brief_sinsdra[i];
  //     items.add(buildPageViewItem(context, obj));
  //   }

  //   return items;
  // }

}
