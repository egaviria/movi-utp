part of app.ui.views;

class HomeCardRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<HomeViewModel>.reactive(
      builder: (context, model, child) => Scaffold(
          backgroundColor: Colors.grey[200],
          appBar: AppBar(
            shadowColor: Colors.transparent,
            backgroundColor: MyColors.primary,
            title: Text('MOVI'),
          ),
          body: Container(
            // color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Stack(
                  children: [
                    Container(
                      width: double.infinity,
                      padding: EdgeInsets.symmetric(vertical: 16.0),
                      // decoration: BoxDecoration(
                      //   color: MyColors.primary,
                      //   borderRadius: BorderRadius.only(
                      //       bottomLeft: Radius.circular(40.0),
                      //       bottomRight: Radius.circular(40.0)),
                      // ),
                      height: 250.0,
                      child: Image.asset('assets/images/lung_1080.png'),
                    ),
                  ],
                ),
                Padding(
                    padding: EdgeInsets.symmetric(vertical: 12.0),
                    child: Text('Monitoría de la ventilación mecánica ideal',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20.0,
                            color: Colors.grey[800]))),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.all(16.0),
                    child: ListView(
                      // children: [],
                      children: model.getHomeCategory(context),
                    ),
                  ),
                ),
                Container(
                  // color: Colors.red.withOpacity(0.4),
                  alignment: Alignment.centerRight,
                  // padding: EdgeInsets.all(16.0),
                  width: double.infinity,
                  child: FlatButton(
                    onPressed: () {
                      Navigator.pushNamed(context, 'about');
                    },
                    child: Text(
                      'Acerca de',
                      style: MyText.medium(context).copyWith(
                          color: Colors.grey, fontWeight: FontWeight.bold),
                    ),
                    // color: Colors.transparent,
                  ),
                )
              ],
            ),
          )),
      viewModelBuilder: () => HomeViewModel(),
    );
  }
}
