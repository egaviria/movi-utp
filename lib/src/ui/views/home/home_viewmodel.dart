part of app.ui.views;

class HomeViewModel extends BaseViewModel {
  final NavigationService _navigationService = locator<NavigationService>();
  static const List<String> home_card_title = [
    'Programación inicial',
    'Monitorìa'
  ];
  static const List<String> home_card_subtitle = [
    'Programación inicial de la ventilación mecánica',
    'Monitorìa de la ventilación mecánica '
  ];
  static const List<String> home_card_route = [
    Routes.stepperRoute,
    Routes.monitoriaRoute
  ];
  static const List<IconData> home_card_icon = [
    Icons.devices,
    Icons.accessibility,
  ];

  List<Card> getHomeCategory(BuildContext context) {
    List<Card> items = [];
    for (int i = 0; i < home_card_title.length; i++) {
      Card obj = Card(
        child: GestureDetector(
          onTap: () {
            _navigationService.navigateTo(home_card_route[i]);
          },
          child: Padding(
            padding: EdgeInsets.all(16.0),
            child: ListTile(
              leading: Icon(
                home_card_icon[i],
                color: Colors.teal[300],
              ),
              title: Text(home_card_title[i]),
              subtitle: Text(home_card_subtitle[i]),
            ),
          ),
        ),
      );
      items.add(obj);
    }
    return items;
  }
}
