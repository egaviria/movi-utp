part of app.ui.views;

class MonitoriaRouteViewModel extends BaseViewModel {
  final NavigationService _navigationService = locator<NavigationService>();

  final Widget svgMonitoria = SvgPicture.asset(
      'assets/svg/undraw_Surveillance_re_8tkl.svg',
      semanticsLabel: 'Movic Home');

  final optionsTitle = [
    'Gasométrica',
    'Ventilaciòn y Poder Mecánico',
    'Asincronias',
    'Cambio de PaCO2 e IMV ideal y Monitorización de CO2',
  ];

  final optionsRoute = [
    Routes.gasometricaRoute,
    Routes.parametrosPresionRoute,
    Routes.asincronasRoute,
    Routes.cambiodePaCO2Route,
  ];

  static const List<IconData> optionsIcon = [
    Icons.devices,
    Icons.panorama_fish_eye,
    Icons.format_align_justify,
    Icons.record_voice_over,
  ];

  static const List<MaterialColor> optionsColor = [
    Colors.pink,
    Colors.blue,
    Colors.amber,
    Colors.lightGreen,
  ];

  List<Card> getListItems() {
    List<Card> items = [];
    for (int i = 0; i < optionsTitle.length; i++) {
      Card obj = Card(
        child: GestureDetector(
          onTap: () {
            print(optionsRoute[i]);
            _navigationService.navigateTo(optionsRoute[i]);
          },
          child: Padding(
            padding: EdgeInsets.all(16.0),
            child: ListTile(
              leading: Container(
                  padding: EdgeInsets.all(10.0),
                  decoration: BoxDecoration(
                      color: optionsColor[i],
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  child: Icon(
                    optionsIcon[i],
                    color: Colors.white,
                  )),
              title: Text(optionsTitle[i]),
              // subtitle: Text(home_card_subtitle[i]),
            ),
          ),
        ),
      );
      items.add(obj);
    }
    return items;
  }
}
