part of app.ui.views;

class MonitoriaRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder.reactive(
        builder: (context, MonitoriaRouteViewModel model, child) => Scaffold(
              appBar: AppBar(
                title: Text('Monitoria'),
              ),
              body: Container(
                padding: EdgeInsets.all(16.0),
                child: Column(
                    // crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Column(
                        children: [
                          Container(
                            padding: EdgeInsets.all(30.0),
                            decoration: BoxDecoration(),
                            height: 200.0,
                            child: model.svgMonitoria,
                          ),
                        ],
                      ),
                      Expanded(
                        child: ListView(
                          // children: [],
                          children: model.getListItems(),
                        ),
                      ),
                    ]),
              ),
            ),
        viewModelBuilder: () => MonitoriaRouteViewModel());
  }
}
