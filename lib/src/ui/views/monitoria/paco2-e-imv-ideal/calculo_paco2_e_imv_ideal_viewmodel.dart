part of app.ui.views;

class CalculoCambiodePaCO2ViewModel extends BaseViewModel {
  final _cambiodePaCO2Service = locator<CambiodePaCO2Service>();

  double get espVt => _cambiodePaCO2Service.espVt;
  double get espf => _cambiodePaCO2Service.espF;
  double get paCO2ETCO2 => _cambiodePaCO2Service.paCO2ETCO2;
  double get vdvt => _cambiodePaCO2Service.vdvt;
}
