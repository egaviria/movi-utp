part of app.ui.views;

class CambiodePaCO2Route extends StatelessWidget {
  const CambiodePaCO2Route({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<CambiodePaCO2ViewModel>.reactive(
        builder: (context, model, child) => Scaffold(
            appBar: AppBar(
              title:
                  Text('Cambio de PaCO2 e IMV ideal\ny Monitorización de CO2'),
            ),
            body: Padding(
              padding: EdgeInsets.symmetric(horizontal: 24.0),
              child: ListView(
                children: [
                  SizedBox(
                    height: 16,
                  ),
                  Form(
                    key: model._formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              labelText: 'Edad del Paciente',
                              suffix: Text('Años'),
                            ),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Campo requerido';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              model._edad = int.parse(value);
                            }),
                        SizedBox(height: 16),
                        TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: 'ETCO2',
                                suffix: Text('mmHg'),
                                helperText: 'CO2 al final de la espiración'),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Campo requerido';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              model._eTCO2 = double.parse(value);
                            }),
                        TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: 'PaCO2 act',
                                suffix: Text('mmHg'),
                                helperText:
                                    'Presión arterial de dióxido de carbono actual'),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Campo requerido';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              model._actPaCO2 = double.parse(value);
                            }),
                        TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: 'PaCO2 esp',
                                suffix: Text('mmHg'),
                                helperText:
                                    'Presión arterial de dióxido de carbono esperada'),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Campo requerido';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              model._espPaCO2 = double.parse(value);
                            }),
                        SizedBox(height: 16),
                        TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: 'Vt actual',
                                suffix: Text('mL'),
                                helperText: 'Volumen tidal'),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Campo requerido';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              model._vT = double.parse(value);
                            }),
                        SizedBox(height: 16),
                        TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: 'f actual',
                                suffix: Text('rpm'),
                                helperText: 'Frecuencia respiratoria'),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Campo requerido';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              model._f = double.parse(value);
                            }),
                        SizedBox(
                          height: 16,
                        ),
                        Center(
                          child: RaisedButton(
                            color: Colors.cyan,
                            onPressed: () {
                              // Validate returns true if the form is valid, or false
                              // otherwise.
                              if (model._formKey.currentState.validate()) {
                                model._formKey.currentState.save();
                                // If the form is valid, display a Snackbar.

                                model.submit();
                              }
                            },
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 16),
                              child: Text(
                                'Calcular',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                        UISpacing.spacingV16
                      ],
                    ),
                  )
                ],
              ),
            )),
        viewModelBuilder: () => CambiodePaCO2ViewModel());
  }
}
