part of app.ui.views;

class CalculoCambiodePaCO2Route extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<CalculoCambiodePaCO2ViewModel>.reactive(
      builder: (context, model, child) => Scaffold(
        backgroundColor: UIColors.cyanMedium,
        appBar: AppBar(
          title: Text('Cambio de PaCO2 e IMV ideal\ny Monitorización de CO2'),
        ),
        body: ListView(
          padding: EdgeInsets.zero,
          children: [
            Column(
              children: [
                UISpacing.spacingV16,
                Padding(
                  padding: UIPadding.paddingH16,
                  child: Text(
                    'Antes de cualquier corrección de CO2 tener en cuenta que todo alveólo necesita alrededor de 3 constantes de tiempo alveolar para su adecuado vaciamiento',
                    style: TextStyle(
                        color: Colors.black87,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                ),
                UISpacing.spacingV16,
                UiCards.displayCard(
                  false,
                  leadingText: 'Vt esp',
                  subLeadingText: 'Volumen tidal esperado',
                  measurement: 'mL',
                  trailingText: model.espVt.toStringAsFixed(2),
                ),
                UISpacing.spacingV16,
                UiCards.displayCard(
                  false,
                  leadingText: 'f esp',
                  subLeadingText: 'Frecuencia respiratoria esperada',
                  measurement: 'rpm',
                  trailingText: model.espf.toStringAsFixed(2),
                ),
                UISpacing.spacingV16,
                UiCards.displayCard(
                  false,
                  leadingText: 'PaCO2/ETCO2',
                  subLeadingText: 'Gradiente',
                  measurement: 'mmHg',
                  vrText: '< 5',
                  errorMsg: (model.paCO2ETCO2 > 5)
                      ? 'Aumento de espacio muerto anatómico,\naumento de espacio muerto fisiológico,\no estados de bajo gasto.'
                      : null,
                  trailingText: model.paCO2ETCO2.toStringAsFixed(2),
                ),
                UISpacing.spacingV16,
                UiCards.displayCard(
                  false,
                  leadingText: 'Vd/vt',
                  subLeadingText: 'Por ecuación simplificada',
                  trailingText: model.vdvt.toStringAsFixed(2),
                  vrText: '< 0.55',
                  errorMsg: (model.vdvt > 0.55)
                      ? 'Considerar aumento de espacio muerto\n fisiológico+Shunt. Precaución al extubar.\n Complementar con Capnografía volumétrica.'
                      : null,
                ),
                UISpacing.spacingV16,
              ],
            )
          ],
        ),
      ),
      viewModelBuilder: () => CalculoCambiodePaCO2ViewModel(),
    );
  }
}
