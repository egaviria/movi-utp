part of app.ui.views;

class CambiodePaCO2ViewModel extends BaseViewModel {
  final NavigationService _navigationService = locator<NavigationService>();
  final _formKey = GlobalKey<FormState>();
  final _cambiodePaCO2Service = locator<CambiodePaCO2Service>();

  int _edad;
  double _eTCO2;
  double _actPaCO2;
  double _espPaCO2;
  double _vT;
  double _f;

  void submit() {
    _cambiodePaCO2Service.espVtResult = (_vT * _actPaCO2) / _espPaCO2;
    _cambiodePaCO2Service.espfResult = (_f * _actPaCO2) / _espPaCO2;
    _cambiodePaCO2Service.paCO2ETCO2Result = (_actPaCO2 - _eTCO2);
    _cambiodePaCO2Service.vdvtResult = (0.32 +
        (0.0106 * (_actPaCO2 - _eTCO2)) +
        0.003 * (_f) +
        0.0015 * (_edad));

    notifyListeners();
    _navigationService.navigateTo(Routes.calculoCambiodePaCO2Route);
  }
}
