part of app.ui.views;

class AsincronasRoute extends StatelessWidget {
  const AsincronasRoute({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<AsincronasViewModel>.reactive(
        builder: (context, model, child) => Scaffold(
              appBar: AppBar(
                title: Text('Asincronías'),
              ),
              body: Padding(
                padding: UIPadding.paddingH16,
                child: ListView(
                  children: [
                    UiCards.displayCardTwo(
                        'assets/images/asincronia_esfuerzo_inefectivo.png',
                        function: () async {
                      model._options = model._asincronias['esfuerzo'];
                      await model.showBasicBottomSheet();
                    },
                        titleText: 'Asincronía por Esfuerzo Inefectivo',
                        descriptionText:
                            '- Paciente hace esfuerzo y curva de P hace deflexión  negativa sin disparo, curva de flujo muestra disminución abrupta flujo espiratorio'),
                    UiCards.displayCardTwo(
                        'assets/images/asincronia_autodisparo.png',
                        function: () async {
                      model._options = model._asincronias['autoDisparo'];
                      await model.showBasicBottomSheet();
                    },
                        titleText: 'Asincronía por Auto-Disparo',
                        descriptionText:
                            '- Respiraciones no iniciadas por el paciente: por baja sensibilidad, fuga de circuito o estado hiperdinámico'),
                    UiCards.displayCardTwo(
                        'assets/images/asincronia_sed_flujo.png',
                        function: () async {
                      model._options = model._asincronias['sedFlujo'];
                      await model.showBasicBottomSheet();
                    },
                        titleText: 'Asincronía por Sed de Flujo',
                        descriptionText:
                            '- El flujo no es suficiente para las demandas del paciente\n- Se ve una concavidad en la curva presión tiempo'),
                    UiCards.displayCardTwo(
                        'assets/images/asincronia_ciclado_temprano.png',
                        function: () async {
                      model._options = model._asincronias['cicladoTemprano'];
                      await model.showBasicBottomSheet();
                    },
                        titleText: 'Asincronía por Ciclado Temprano',
                        descriptionText:
                            '- La duración de la ventilación mecánica es más corta que e esfuerzo inspiratorio del paciente'),
                    UiCards.displayCardTwo(
                        'assets/images/asincronia_ciclado_tardio.png',
                        function: () async {
                      model._options = model._asincronias['cicladoTardio'];
                      await model.showBasicBottomSheet();
                    },
                        titleText: 'Asincronía por Ciclado Tardío',
                        descriptionText:
                            '- El tiempo de inspiración mecánico sobrepasa el tiempo de inspiración neural\n- Aumento Presión al final de la espiración causada por contracción de músculos espiratorios'),
                    UiCards.displayCardTwo(
                        'assets/images/asincronia_doble_disparo.png',
                        function: () async {
                      model._options = model._asincronias['dobleDisparo'];
                      await model.showBasicBottomSheet();
                    },
                        titleText: 'Asincronía por Doble Disparo',
                        descriptionText:
                            '- Tiempo neural del paciente es largo y el del ventilador es corto\n- Consecuencia de otras asincronías :Por ciclado temprano, auto-disparo o  que el ciclado sea muy alto, drive respiratorio alto\n- Se ve doble disparo en la curva de flujo')
                  ],
                ),
              ),
            ),
        viewModelBuilder: () => AsincronasViewModel());
  }
}
