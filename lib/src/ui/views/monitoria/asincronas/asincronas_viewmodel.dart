part of app.ui.views;

class AsincronasViewModel extends BaseViewModel {
  String _options;

  final Map _asincronias = {
    'esfuerzo': '''
- Verificar trigger del ventilador --> en ese caso disminuirla.

- Descartar atrapamiento de aire excesivo compensando el autoPEEP con PEEP externo.

- Verificar asistencia; ver si tiene p. soporte muy alta para evitar la sobreasistencia.

- Considerar cambiar a una ventilación-asistida-proporcional.
     ''',
    'autoDisparo': '''
- Verificar trigger en ese caso aumentarlo.

- Verificar si hay fugas en el circuito del ventilador.

- Descartar estado hiperdinámico antes de decir que el paciente tiene autotrigger.
 ''',
    'sedFlujo': '''
- Aumentar flujo inspiratorio o disminuir tiempo inspiratorio.

- Otros autores recomiendan cambiar a modo por presión donde el flujo se ajusta al paciente de acuerdo a sus demandas.

 ''',
    'cicladoTemprano': '''
- Aumentar tiempo inspiratorio.
 ''',
    'cicladoTardio': '''
- Disminuir tiempo inspiratorio.

- En modos por presión se modifica la variable de ciclado o trigger espiratorio.

- Usar modos proporcionales. 

 ''',
    'dobleDisparo': '''
- Aumentar t. inspiratorio.

- Prueba con presión soporte ajustando criterios de terminación de flujo  o usar modos proporcionales.

- Paralizantes  cuando se trata de un SDRA.
 '''
  };

  final _bottomSheetService = locator<BottomSheetService>();

  Future showBasicBottomSheet() async {
    await _bottomSheetService.showBottomSheet(
        title: 'Acciones', description: '$_options');
  }
}
