part of app.ui.views;

class ParametrosPresionViewModel extends BaseViewModel {
  final _presionService = locator<PresionService>();

  final NavigationService _navigationService = locator<NavigationService>();
  final _formKey = GlobalKey<FormState>();

  double _f;
  double _vt;
  double _pip;
  double _ppl;
  // double _pmed;
  double _peep;
  double _flujoInspiratorio;

  void parseF(value) {
    _f = double.parse(value);
    notifyListeners();
  }

  void parseVt(value) {
    _vt = double.parse(value);
    notifyListeners();
  }

  void parsePIP(value) {
    _pip = double.parse(value);
    notifyListeners();
  }

  void parsePpL(value) {
    _ppl = double.parse(value);
    notifyListeners();
  }

  // void parsePmed(value) {
  //   _pmed = double.parse(value);
  //   notifyListeners();
  // }

  void parsePEEP(value) {
    _peep = double.parse(value);
    notifyListeners();
  }

  void parseFlujo(value) {
    _flujoInspiratorio = double.parse(value);
    notifyListeners();
  }

  void _getCalculos() {
    _presionService.rResult = ((_pip - _ppl) / (_flujoInspiratorio / 60));

    _presionService.cstatResult = (_vt / (_ppl - _peep));
    _presionService.cdynResult = (_vt / (_pip - _peep));
    _presionService.eResult = ((1 / _presionService.cstat) * 1000);
    _presionService.drivingPresureResult = (_ppl - _peep);
    _presionService.volMinutoResult = ((_f * _vt) / 1000);
    _presionService.poderMecanicoResult = ((_presionService.volMinuto *
            (_pip + _peep + (_flujoInspiratorio / 6))) /
        20);

    _navigationService.navigateTo(
      Routes.calculoPresionRoute,
    );
  }
}
