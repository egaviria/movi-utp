part of app.ui.views;

class CalculoPresionViewModel extends ParametrosPresionViewModel {
  double get r => _presionService.r;
  double get cstat => _presionService.cstat;
  double get cdyn => _presionService.cdyn;
  double get e => _presionService.e;
  double get drivingPresure => _presionService.drivingPresure;
  double get volumenMinuto => _presionService.volMinuto;
  double get poderMecanico => _presionService.poderMecanico;
}
