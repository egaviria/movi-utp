part of app.ui.views;

class ParametrosPresionRoute extends StatelessWidget {
  const ParametrosPresionRoute({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<ParametrosPresionViewModel>.reactive(
        builder: (context, ParametrosPresionViewModel model, child) => Scaffold(
            appBar: AppBar(
              title: Text('Monitoreo ventilaciòn y poder mecánico'),
            ),
            body: Padding(
              padding: EdgeInsets.symmetric(horizontal: 24.0),
              child: ListView(
                children: [
                  Container(
                      padding: EdgeInsets.only(top: 16.0),
                      alignment: Alignment.center,
                      child: Text(
                        'Parametros de Presion',
                        style: h3,
                      )),
                  UISpacing.spacingV16,
                  Form(
                    key: model._formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextFormField(
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              labelText: 'f',
                              suffix: Text('rpm'),
                              helperText: 'frecuencia respiratoria'),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Campo requerido';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            model.parseF(value);
                          },
                        ),
                        UISpacing.spacingV16,
                        TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: 'Vt',
                                suffix: Text('mL'),
                                helperText: 'volumen corriente'),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Campo requerido';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              model.parseVt(value);
                            }),
                        UISpacing.spacingV16,
                        TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: 'PIP',
                                suffix: Text('CmH2O'),
                                helperText: 'presión inspiratoria pico'),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Campo requerido';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              model.parsePIP(value);
                            }),
                        UISpacing.spacingV16,
                        TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: 'PpL',
                                suffix: Text('CmH2O'),
                                helperText: 'presión plateau'),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Campo requerido';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              model.parsePpL(value);
                            }),
                        UISpacing.spacingV16,
                        TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: 'Pmed',
                                suffix: Text('CmH2O'),
                                helperText: 'presión media de la vía aérea'),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Campo requerido';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              // model.parsePmed(value);
                            }),
                        UISpacing.spacingV16,
                        TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: 'PEEP total',
                                suffix: Text('CmH2O'),
                                helperText:
                                    'presión positiva al final de la espiración'),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Campo requerido';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              model.parsePEEP(value);
                            }),
                        UISpacing.spacingV16,
                        TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              labelText: 'flujo inspiratorio',
                              suffix: Text('L/min'),
                            ),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Campo requerido';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              model.parseFlujo(value);
                            }),
                        UISpacing.spacingV16,
                        Center(
                          child: RaisedButton(
                            color: Colors.cyan,
                            onPressed: () {
                              // Validate returns true if the form is valid, or false
                              // otherwise.
                              if (model._formKey.currentState.validate()) {
                                model._formKey.currentState.save();
                                // If the form is valid, display a Snackbar.

                                model._getCalculos();
                              }
                            },
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 16),
                              child: Text(
                                'Calcular',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                        UISpacing.spacingV16
                      ],
                    ),
                  )
                ],
              ),
            )),
        viewModelBuilder: () => ParametrosPresionViewModel());
  }
}
