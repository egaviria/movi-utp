part of app.ui.views;

class CalculoPresionRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<CalculoPresionViewModel>.reactive(
        builder: (context, model, child) => Scaffold(
              appBar: AppBar(
                title: Text('Monitoreo ventilaciòn y poder mecánico'),
              ),
              body: ListView(
                children: [
                  UISpacing.spacingV24,
                  Text(
                    'Mecánica Respiratoria',
                    style: h2,
                    textAlign: TextAlign.center,
                  ),
                  UISpacing.spacingV16,
                  UiCards.displayCard(false,
                      leadingText: 'R',
                      vrText: '0,6-6',
                      subLeadingText: 'Resistencia de la vía aérea',
                      trailingText: model.r.toStringAsFixed(2),
                      measurement: 'cmH2O/Lt/seg',
                      errorMsg: (model.r > 0.6 && model.r > 6)
                          ? 'Alterado:\n¿El paciente se encuentra\nmordiendo el tubo?\n¿hay secreciones en la vía aére?\n¿hay broncoconstricción?'
                          : 'Alterado'),
                  UiCards.displayCard(false,
                      leadingText: 'Cstat',
                      vrText: '60-100',
                      subLeadingText: 'Compliance estática',
                      trailingText: model.cstat.toStringAsFixed(2),
                      measurement: 'mL/cmH2',
                      errorMsg: (model.cstat < 60 || model.cstat > 100)
                          ? 'Alterado'
                          : null),
                  UiCards.displayCard(false,
                      leadingText: 'Cdyn',
                      vrText: '50-80',
                      subLeadingText: 'Compliance dinámica',
                      trailingText: model.cdyn.toStringAsFixed(2),
                      measurement: 'mL/cmH2',
                      errorMsg: (model.cdyn < 50 || model.cdyn > 80)
                          ? 'Alterado'
                          : null),
                  UiCards.displayCard(false,
                      leadingText: 'E',
                      vrText: '10-17',
                      subLeadingText: 'Elastancia',
                      trailingText: model.e.toStringAsFixed(2),
                      measurement: 'cmH2O/Lt',
                      errorMsg:
                          (model.e < 10 || model.e > 17) ? 'Alterado' : null),
                  UiCards.displayCard(false,
                      leadingText: 'Driving Pressure',
                      vrText: '<14',
                      subLeadingText: '',
                      trailingText: model.drivingPresure.toStringAsFixed(2),
                      errorMsg: (model.drivingPresure > 14)
                          ? 'Alterado:\nconsidere disminuir el volumen corriente'
                          : null),
                  UiCards.displayCard(
                    false,
                    leadingText: 'Volumen por minuto',
                    vrText: '',
                    subLeadingText: '',
                    trailingText: model.volumenMinuto.toStringAsFixed(2),
                  ),
                  UiCards.displayCard(false,
                      leadingText: 'Poder Mecánico',
                      vrText: '<17',
                      subLeadingText: '',
                      measurement: 'Joules/min',
                      trailingText: model.poderMecanico.toStringAsFixed(2),
                      errorMsg: (model.poderMecanico > 12)
                          ? 'Alterado:  Considere ajustar parámetros de la ventilación mecánica\npara evitar el daño alveolar\ninducido por el ventilador'
                          : null),
                  UISpacing.spacingV24,
                ],
              ),
            ),
        viewModelBuilder: () => CalculoPresionViewModel());
  }
}
