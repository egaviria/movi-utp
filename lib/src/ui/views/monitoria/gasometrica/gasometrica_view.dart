part of app.ui.views;

class GasometricaRoute extends StatelessWidget {
  const GasometricaRoute({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<GasometricaRouteViewModel>.reactive(
        builder: (context, model, child) => Scaffold(
            appBar: AppBar(
              title: Text('Monitorìa Gasomètrica'),
            ),
            body: Padding(
              padding: EdgeInsets.symmetric(horizontal: 24.0),
              child: ListView(
                children: [
                  Container(
                      padding: EdgeInsets.only(top: 16.0),
                      alignment: Alignment.center,
                      child: Text(
                        'Análisis de oxigenación y perfusión',
                        style: h3,
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Form(
                    key: model._formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextFormField(
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              labelText: 'PBar',
                              suffix: Text('mmHg'),
                              helperText: 'Presión barométrica'),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Campo requerido';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            model.parsePbar(value);
                          },
                        ),
                        SizedBox(height: 16),
                        TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: 'FiO2',
                                suffix: Text('%'),
                                helperText: 'Fracción inspirada de oxígeno'),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Campo requerido';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              model.parseFiO2(value);
                            }),
                        SizedBox(height: 16),
                        TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: 'Hb',
                                suffix: Text('gr/dl'),
                                helperText: 'hemoglobina'),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Campo requerido';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              model.parseHb(value);
                            }),
                        SizedBox(height: 16),
                        TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: 'PaO2',
                                suffix: Text('mmHg'),
                                helperText: 'Presión arterial de oxígeno'),
                            onSaved: (value) {
                              if (value.isEmpty) {
                                return model._paO2 = 0.0;
                              } else {
                                model.parsePaO2(value);
                              }
                            }),
                        SizedBox(height: 16),
                        TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: 'PaCO2',
                                suffix: Text('mmHg'),
                                helperText:
                                    'Presión arterial de Dióxido de carbono'),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Campo requerido';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              model.parsePaCO2(value);
                            }),
                        SizedBox(height: 16),
                        TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: 'SaO2',
                                suffix: Text('%'),
                                helperText: 'saturación arterial de oxígeno '),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Campo requerido';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              model.parseSaO2(value);
                            }),
                        SizedBox(height: 16),
                        TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: 'PvO2',
                                suffix: Text('mmHg'),
                                helperText: 'presión venosa de oxígeno'),
                            onSaved: (value) {
                              if (value.isEmpty) {
                                return model._pvO2 = 0.0;
                              } else {
                                model.parsePvO2(value);
                              }
                            }),
                        SizedBox(height: 16),
                        TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: 'PvCO2',
                                suffix: Text('mmHg'),
                                helperText:
                                    'presión venosa de dióxido de carbono'),
                            onSaved: (value) {
                              if (value.isEmpty) {
                                return model._pvCO2 = 0.0;
                              } else {
                                model.parsePvCO2(value);
                              }
                            }),
                        SizedBox(height: 16),
                        TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: 'SvO2',
                                suffix: Text('%'),
                                helperText: 'Saturación venosa de Oxígeno'),
                            onSaved: (value) {
                              if (value.isEmpty) {
                                return model._gasometricaService.svO2Result =
                                    0.0;
                              } else {
                                model.parseSvO2(value);
                              }
                            }),
                        SizedBox(
                          height: 16,
                        ),
                        Center(
                          child: RaisedButton(
                            color: Colors.cyan,
                            onPressed: () {
                              // Validate returns true if the form is valid, or false
                              // otherwise.
                              if (model._formKey.currentState.validate()) {
                                model._formKey.currentState.save();
                                // If the form is valid, display a Snackbar.

                                model._getCalculosIntermedios();
                              }
                            },
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 16),
                              child: Text(
                                'Calcular',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                        UISpacing.spacingV16
                      ],
                    ),
                  )
                ],
              ),
            )),
        viewModelBuilder: () => GasometricaRouteViewModel());
  }
}
