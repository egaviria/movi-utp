part of app.ui.views;

class CalculoGasometricaViewModel extends BaseViewModel {
  final _gasometricaService = locator<GasometricaService>();

  double get dAaO2 => _gasometricaService.dAaO2;
  double get iaA => _gasometricaService.iaA;

  double get pAFI => _gasometricaService.pAFI;

  double get qsQt => _gasometricaService.qsQt;
  double get saFi => _gasometricaService.saFi;
  double get gadrey => _gasometricaService.gadrey;

  double get paO2 => _gasometricaService.paO2;
  double get davO2 => _gasometricaService.davO2;

  double get extO2 => _gasometricaService.extO2;

  double get svO2 => _gasometricaService.svO2;
  double get deltaPCO2 => _gasometricaService.deltaPCO2;

  String safiValidation() {
    if (saFi < 315 && saFi >= 235) {
      return 'Trastorno leve de oxigenación';
    } else if (saFi < 235) {
      return 'Trastorno moderado de oxigenación';
    } else {
      return null;
    }
  }

  // List<Widget> cardList = [];
  // int counter = 0;

  // final GlobalKey<AnimatedListState> _listKey = GlobalKey<AnimatedListState>();

  // Tween<Offset> _offset = Tween(begin: Offset(0, 1), end: Offset(0, 0));

  // Widget slideIt(BuildContext context, int index, animation) {
  //   return SlideTransition(
  //       position: Tween<Offset>(
  //         begin: const Offset(-1, 0),
  //         end: Offset(0, 0),
  //       ).animate(animation),
  //       child: cardList[index]);
  // }
}
