part of app.ui.views;

class CalculoGasometricaRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<CalculoGasometricaViewModel>.reactive(
        onModelReady: (CalculoGasometricaViewModel model) =>
            model.safiValidation(),
        builder: (context, model, child) => Scaffold(
              appBar: AppBar(
                title: Text('Monitorìa Gasomètrica'),
              ),
              body: ListView(
                padding: EdgeInsets.zero,
                children: [
                  Container(
                    color: UIColors.cyanMedium,
                    child: Column(
                      children: [
                        UISpacing.spacingV16,
                        Text(
                          'Oxigenación',
                          style: h2,
                          textAlign: TextAlign.center,
                        ),
                        UISpacing.spacingV8,
                        UiCards.displayCard(false,
                            leadingText: 'D(A-a)O2',
                            subLeadingText:
                                'gradiente alveolo\narterial de oxígeno',
                            trailingText: model.dAaO2.toStringAsFixed(2),
                            vrText: '< 10',
                            errorMsg: !(model.dAaO2 > 10) ? 'Alterado' : null),
                        UiCards.displayCard(false,
                            leadingText: 'IaA',
                            subLeadingText: 'índice arterio alveolar',
                            vrText: '0,7-0,9',
                            trailingText: model.iaA.toStringAsFixed(2),
                            errorMsg: (model.iaA < 0.7 || model.iaA > 0.9)
                                ? 'Alterado'
                                : null),
                        UiCards.displayCard(false,
                            leadingText: 'PAFI',
                            vrText: '> 400',
                            subLeadingText:
                                'Presión arterial de oxígeno\ny la fracción inspirada de oxígeno',
                            trailingText: model.pAFI.toStringAsFixed(2),
                            errorMsg: (model.pAFI < 100)
                                ? 'Trastorno severo de oxigenación'
                                : (model.pAFI < 400 && model.pAFI > 100)
                                    ? 'Trastorno de oxigenación'
                                    : null),
                        UiCards.displayCard(false,
                            leadingText: 'Qs/Qt',
                            vrText: '< 10%',
                            subLeadingText: 'Porcentaje de Shunt',
                            trailingText: model.qsQt.toStringAsFixed(2),
                            errorMsg: (model.qsQt > 10)
                                ? 'Aumento del SHUNT considere\noptimizar el PEEP'
                                : null),
                        UISpacing.spacingV16,
                        UiCards.displayCard(
                          false,
                          leadingText: 'PaO2',
                          subLeadingText: 'Presión arterial de oxígeno',
                          trailingText: model.paO2.toStringAsFixed(2),
                        ),
                        UISpacing.spacingV16,
                        UiCards.displayCard(
                          false,
                          leadingText: 'SaFi',
                          vrText: '> 315',
                          errorMsg: model.safiValidation(),
                          subLeadingText: 'Presión arterial de oxígeno',
                          trailingText: model.saFi.toStringAsFixed(2),
                        ),
                        UISpacing.spacingV16,
                        UiCards.displayCard(
                          false,
                          leadingText: 'Fórmula de Gadrey',
                          subLeadingText: 'Equivalencia de SaFi a PaFi',
                          trailingText: model.gadrey.toStringAsFixed(2),
                        ),
                        UISpacing.spacingV16
                      ],
                    ),
                  ),
                  Container(
                    color: UIColors.redMedium,
                    child: Column(
                      children: [
                        UISpacing.spacingV16,
                        Text(
                          'Perfusión',
                          style: h2,
                          textAlign: TextAlign.center,
                        ),
                        UISpacing.spacingV8,
                        UiCards.displayCard(true,
                            leadingText: 'D(a-v)O2',
                            vrText: '3-5',
                            subLeadingText:
                                'Gradiente arterio-venoso\nde oxígeno',
                            trailingText: model.davO2.toStringAsFixed(2),
                            errorMsg: (model.davO2 > 5)
                                ? 'Trastorno perfusión'
                                : (model.davO2 < 3)
                                    ? 'Alterado'
                                    : null),
                        UiCards.displayCard(true,
                            leadingText: 'ExtO2',
                            vrText: '20-30',
                            subLeadingText: 'Tasa de extracción',
                            trailingText: model.extO2.toStringAsFixed(2),
                            errorMsg: (model.extO2 > 30)
                                ? 'Trastorno perfusión\naumento de extracción'
                                : (model.extO2 < 20)
                                    ? 'Alterado'
                                    : null),
                        UiCards.displayCard(true,
                            leadingText: 'SvO2',
                            vrText: '70-80',
                            subLeadingText: 'Saturación venosa de oxígeno',
                            trailingText: model.svO2.toStringAsFixed(2),
                            errorMsg: (model.svO2 < 70)
                                ? 'Trastorno perfusión'
                                : (model.svO2 > 80)
                                    ? 'Alterado'
                                    : null),
                        UiCards.displayCard(true,
                            leadingText: 'DeltaCO2',
                            vrText: '<7',
                            subLeadingText: 'Diferencia arterio-venosa de CO2',
                            trailingText: model.deltaPCO2.toStringAsFixed(2),
                            errorMsg: (model.deltaPCO2 > 6)
                                ? 'Trastorno perfusión\nprobable disfunción de bomba'
                                : null),
                        UISpacing.spacingV16
                      ],
                    ),
                  )
                ],
              ),
            ),
        viewModelBuilder: () => CalculoGasometricaViewModel());
  }
}
