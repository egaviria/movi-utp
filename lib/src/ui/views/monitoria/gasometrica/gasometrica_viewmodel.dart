part of app.ui.views;

class GasometricaRouteViewModel extends BaseViewModel {
  final NavigationService _navigationService = locator<NavigationService>();
  final _formKey = GlobalKey<FormState>();
  final _gasometricaService = locator<GasometricaService>();

  double _pBar;
  double _fiO2;
  double _hb;
  double _paO2;
  double _paCO2;
  double _saO2;
  double _pvO2;
  double _pvCO2;

  double _pAO2;
  double _ccO2;
  double _caO2;
  double _cvO4;

  void parsePbar(value) {
    _pBar = double.parse(value);
    notifyListeners();
  }

  void parseFiO2(value) {
    _fiO2 = double.parse(value);
    notifyListeners();
  }

  void parseHb(value) {
    _hb = double.parse(value);

    notifyListeners();
  }

  void parsePaO2(value) {
    _paO2 = double.parse(value);

    notifyListeners();
  }

  void parsePaCO2(value) {
    _paCO2 = double.parse(value);

    notifyListeners();
  }

  void parseSaO2(value) {
    _saO2 = double.parse(value);

    notifyListeners();
  }

  void parsePvO2(value) {
    _pvO2 = double.parse(value);

    notifyListeners();
  }

  void parsePvCO2(value) {
    _pvCO2 = double.parse(value);

    notifyListeners();
  }

  void parseSvO2(value) {
    var result = double.parse(value);
    _gasometricaService.svO2Result = (result);
    notifyListeners();
  }

  void _getCalculosIntermedios() {
    _saO2 = (_saO2 / 100);
    _fiO2 = (_fiO2 / 100);
    _paO2 = _paO2 == 0.0 ? pow((23400 / ((1 / _saO2) - 0.99)), (1 / 3)) : _paO2;
    _pAO2 = ((_pBar - 47) * _fiO2) - (_paCO2 / 0.8);
    _ccO2 = (_hb * 1.34) + (0.003 * _pAO2);
    _caO2 = (_hb * 1.34 * _saO2) + (0.003 * _paO2);
    _cvO4 = (_hb * 1.34 * (_gasometricaService.svO2 / 100)) + (0.003 * _pvO2);

    //oxigenacion

    _gasometricaService.dAaO2Result = (_pAO2 - _paO2);

    _gasometricaService.iaAResult = ((_paO2 / _pAO2));

    _gasometricaService.pAFIResult = ((_paO2 / _fiO2));

    _gasometricaService.qsQtResult =
        (((_ccO2 - _caO2) / (_ccO2 - _cvO4)) * 100);

    _gasometricaService.paO2Result = _paO2;

    _gasometricaService.gadreyResult = _paO2 / _fiO2;
    _gasometricaService.saFiResult = (_saO2 / _fiO2) * 100;

    //perfusion
    _gasometricaService.davO2Result = (_caO2 - _cvO4);
    _gasometricaService.extO2Result =
        ((_gasometricaService.davO2 / _caO2) * 100);
    _gasometricaService.deltaPCO2Result = (_pvCO2 - _paCO2);
    notifyListeners();
    _navigationService.navigateTo(Routes.calculoGasometricaRoute);
  }
}
