library app.ui.views;

import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get_it/get_it.dart';
import 'package:movi_utp/app/locator.dart';
import 'package:movi_utp/app/router.gr.dart';
import 'package:movi_utp/services/values_service.dart';

import 'package:movi_utp/src/ui/shared/shared.dart';
import 'package:movi_utp/data/img.dart';
import 'package:movi_utp/data/my_colors.dart';
import 'package:movi_utp/model/step.dart';
import 'package:movi_utp/provider/sdra_provider.dart';

import 'package:movi_utp/widget/my_text.dart';

import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import '../shared/shared.dart';

part 'home/home_view.dart';
part 'home/home_viewmodel.dart';
part 'about/about_view.dart';
part 'about/about_viewmodel.dart';
part 'programacion/stepOne/step_one_view.dart';
part 'programacion/stepOne/step_one_viewmodel.dart';
part 'programacion/stepper/stepper_view.dart';
part 'programacion/stepper/stepper_viewmodel.dart';
part 'monitoria/monitoria_view.dart';
part 'monitoria/monitoria_viewmodel.dart';
part 'monitoria/gasometrica/gasometrica_view.dart';
part 'monitoria/gasometrica/gasometrica_viewmodel.dart';
part 'monitoria/gasometrica/calculo_gasometrica_view.dart';
part 'monitoria/gasometrica/calculo_gasometrica_viewmodel.dart';
part 'monitoria/parametrosPresion/parametros_presion_view.dart';
part 'monitoria/parametrosPresion/parametros_presion_viewmodel.dart';
part 'monitoria/parametrosPresion/calculo_presion_view.dart';
part 'monitoria/parametrosPresion/calculo_presion_viewmodel.dart';
part 'monitoria/asincronas/asincronas_viewmodel.dart';
part 'monitoria/asincronas/asincronas_view.dart';

part 'monitoria/paco2-e-imv-ideal/paco2_e_imv_ideal_view.dart';
part 'monitoria/paco2-e-imv-ideal/paco2_e_imv_ideal_viewmodel.dart';
part 'monitoria/paco2-e-imv-ideal/calculo_paco2_e_imv_ideal_view.dart';
part 'monitoria/paco2-e-imv-ideal/calculo_paco2_e_imv_ideal_viewmodel.dart';
