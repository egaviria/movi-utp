import 'package:observable_ish/observable_ish.dart';
import 'package:stacked/stacked.dart';

class SdraProvider with ReactiveServiceMixin {
  // bool _sdra = false;
  // bool get sdra => _sdra;

  SdraProvider() {
    //3
    listenToReactiveValues([_sdra]);
  }

  // 2
  final RxValue<bool> _sdra = RxValue<bool>(initial: false);
  bool get sdra => _sdra.value;
  // set sdra(bool value) => _sdra.value = value;

  void switchSdra() {
    _sdra.value = !_sdra.value;
  }
}
