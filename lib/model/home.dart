import 'package:flutter/cupertino.dart';

class HomeCategory {
  IconData icon;
  String image;
  String title;
  String brief;
}
