import 'package:injectable/injectable.dart';
import 'package:movi_utp/provider/sdra_provider.dart';
import 'package:stacked_services/stacked_services.dart';

@module
abstract class ThirdPartyServicesModule {
  @lazySingleton
  NavigationService get navigationService;
  @lazySingleton
  DialogService get dialogService;
  @lazySingleton
  SdraProvider get sdra;
  @lazySingleton
  BottomSheetService get bottomSheetService;
}
