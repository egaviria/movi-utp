import 'package:injectable/injectable.dart';

@lazySingleton
class GasometricaService {
  double _dAaO2 = 0;
  double _iaA = 0;
  double _pAFI = 0;
  double _qsQt = 0;
  double _paO2 = 0;
  double _saFi = 0;
  double _gadrey = 0;

  double _davO2 = 0;
  double _extO2 = 0;
  double _svO2 = 0;
  double _deltaPCO2 = 0;
  double get dAaO2 => _dAaO2;
  double get iaA => _iaA;
  double get pAFI => _pAFI;
  double get qsQt => _qsQt;
  double get paO2 => _paO2;
  double get saFi => _saFi;
  double get gadrey => _gadrey;

  double get davO2 => _davO2;
  double get extO2 => _extO2;
  double get svO2 => _svO2;
  double get deltaPCO2 => _deltaPCO2;

  set dAaO2Result(result) {
    _dAaO2 = result;
  }

  set iaAResult(result) {
    _iaA = result;
  }

  set pAFIResult(result) {
    _pAFI = result;
  }

  set qsQtResult(result) {
    _qsQt = result;
  }

  set paO2Result(result) {
    _paO2 = result;
  }

  set saFiResult(result) {
    _saFi = result;
  }

  set gadreyResult(result) {
    _gadrey = result;
  }

  set davO2Result(result) {
    _davO2 = result;
  }

  set extO2Result(result) {
    _extO2 = result;
  }

  set svO2Result(result) {
    _svO2 = result;
  }

  set deltaPCO2Result(result) {
    _deltaPCO2 = result;
  }
}

@lazySingleton
class PresionService {
  double _r;
  double _cstat;
  double _cdyn;
  double _e;
  double _drivingPresure;
  double _volMinuto;
  double _poderMecanico;

  double get r => _r;
  double get cstat => _cstat;
  double get cdyn => _cdyn;
  double get e => _e;
  double get drivingPresure => _drivingPresure;
  double get volMinuto => _volMinuto;
  double get poderMecanico => _poderMecanico;

  set rResult(result) {
    _r = result;
  }

  set cstatResult(result) {
    _cstat = result;
  }

  set cdynResult(result) {
    _cdyn = result;
  }

  set eResult(result) {
    _e = result;
  }

  set drivingPresureResult(result) {
    _drivingPresure = result;
  }

  set volMinutoResult(result) {
    _volMinuto = result;
  }

  set poderMecanicoResult(result) {
    _poderMecanico = result;
  }
}

@lazySingleton
class CambiodePaCO2Service {
  double _espVt;
  double _espF;
  double _paCO2ETCO2;
  double _vdvt;

  double get espVt => _espVt;
  double get espF => _espF;
  double get paCO2ETCO2 => _paCO2ETCO2;
  double get vdvt => _vdvt;

  set espVtResult(result) {
    _espVt = result;
  }

  set espfResult(result) {
    _espF = result;
  }

  set paCO2ETCO2Result(result) {
    _paCO2ETCO2 = result;
  }

  set vdvtResult(result) {
    _vdvt = result;
  }
}
