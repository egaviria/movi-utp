import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class SdraProvider extends ChangeNotifier {
  bool _sdra = false;
  bool get sdra => _sdra;

  void switchSdra() {
    _sdra = !_sdra;
  }
}
