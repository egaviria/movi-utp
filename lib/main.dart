import 'package:flutter/material.dart' hide Router;

import 'package:movi_utp/app/locator.dart';
import 'dart:async';

import 'package:movi_utp/app/router.gr.dart';

import 'package:stacked_services/stacked_services.dart';

import 'data/img.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'MOVI',
      home: SplashScreen(),
      onGenerateRoute: Router(),
      navigatorKey: StackedService.navigatorKey,
    );
  }
}

class SplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {
  final NavigationService _navigationService = locator<NavigationService>();
  Future<Timer> startTime() async {
    var duration = Duration(seconds: 2);
    return Timer(duration, navigationPage);
  }

  void navigationPage() {
    _navigationService.replaceWith(Routes.homeCardRoute);
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      // backgroundColor: Colors.white,
      body: Align(
        alignment: Alignment.center,
        child: Container(
          padding: EdgeInsets.all(16.0),
          alignment: Alignment.center,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                width: 400,
                height: 200,
                child:
                    Image.asset(Img.get('logo_utp.png'), fit: BoxFit.contain),
              ),
              // Container(height: 10),
              // Text("MOVI",
              //     style: MyText.headline(context).copyWith(
              //         color: Colors.grey[800], fontWeight: FontWeight.w600)),
            ],
          ),
        ),
      ),
    );
  }
}
